/* Copyright (c) 2015
 * Author: Victor Arribas <v.arribas.urjc@gmail.com>
 * Domain: Master Vision Artificial, URJC
 * License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
 */
 
package varribas.ejercicioiii;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;


public class DescripcionLugar extends ActionBarActivity {

    private TextView description;
    private View layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descripcion_lugar);

        description = (TextView) findViewById(R.id.desc_tvContent);
        layout = (View) findViewById(R.id.desc_layout);

        Intent data = getIntent();
        String xml_file = data.getStringExtra("xml");
        if (xml_file!=null && !xml_file.isEmpty()) {
            if (!parseFile(xml_file)) {
                description.setText("Error :(");
            }
        }else{
            description.setText("None");
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    boolean parseFile(String filename){
        try {
            int id = getResources().getIdentifier(filename, "xml", getPackageName());
            XmlPullParser parser = getResources().getXml(id);

            parseXML(parser);

            return true;
        } catch (XmlPullParserException e) {
            Toast.makeText(getApplicationContext(), "XmlPullParserException", Toast.LENGTH_LONG).show();
            //this.description.setText(e.getMessage());
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), "IOException", Toast.LENGTH_LONG).show();
            //this.description.setText(e.getMessage());
        }
        return false;
    }

    void parseXML(XmlPullParser parser) throws XmlPullParserException,IOException{
        String label = null;
        String value;

        int eventType = parser.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT){
            switch (eventType){
                case XmlPullParser.START_DOCUMENT:
                    break;
                case XmlPullParser.START_TAG:
                    label = parser.getName();
                    switch (label){
                        case "nombre":
                            value = parser.nextText();
                            this.setTitle(value);
                            break;
                        case "descripcion":
                            value = parser.nextText();
                            this.description.setText(value);
                            break;
                        case "imagen":
                            value = parser.nextText();
                            int id = getResources().getIdentifier(value, "drawable", getPackageName());
                            this.layout.setBackgroundResource(id);
                            Drawable image = this.layout.getBackground();
                            image.setAlpha(150);
                            /* API 16+
                            Drawable image = getResources().getDrawable(id);
                            this.layout.setBackground(image);
                            */
                            break;
                    }

                    break;
                case XmlPullParser.END_TAG:
                    label = "";
                    break;
            }

            eventType = parser.next();
        }

    }

}
