/* Copyright (c) 2015
 * Author: Victor Arribas <v.arribas.urjc@gmail.com>
 * Domain: Master Vision Artificial, URJC
 * License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
 */
 
package varribas.ejercicioiii;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;


public class OpcionesLugar extends ActionBarActivity implements View.OnClickListener{
    enum Options{
        DESCRIPCION(R.string.lugarOps_desc),
        MAPA(R.string.lugarOps_map),
        CAMARA(R.string.lugarOps_cam),
        SALIR(R.string.lugarOps_exit);

        public final int strings;
        Options(int s){ strings = s; }
    }

    private View layout;
    private String xml_file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opciones_lugar);

        xml_file = getIntent().getStringExtra("xml");
        String new_title = getIntent().getStringExtra("lugar");
        if (new_title!=null && !new_title.isEmpty())
            this.setTitle(new_title);


        layout = (View) findViewById(R.id.op_layout);

        LinearLayout ly = (LinearLayout) findViewById(R.id.lyButtons);

        int N = Options.values().length;
        for (int i=0; i<N; i++) {
            Options op = Options.values()[i];

            /// Button creation
            Button b = new Button(this);
            b.setText(op.strings);
            b.setOnClickListener(this);

            /// Put into layout
            ly.addView(b);

            /// Add traceability
            b.setTag(op);
        }

        getDataFromXML();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Options op = (Options) v.getTag();
        switch(op){
            case SALIR:
                this.finish();
                break;
            case CAMARA:
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivity(intent);
                break;
            case MAPA:
                Intent i = new Intent(this, MapsActivity.class);
                startActivity(i);
                break;
            case DESCRIPCION:
                i = new Intent(this, DescripcionLugar.class);
                i.putExtra("xml", getIntent().getStringExtra("xml"));
                startActivity(i);
                break;
            default:
                Toast.makeText(this, op.name()+": Unbinded button!", Toast.LENGTH_SHORT).show();
                break;
        }
    }


    private void getDataFromXML(){
        try {
            int id = getResources().getIdentifier(xml_file, "xml", getPackageName());
            XmlPullParser parser = getResources().getXml(id);

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG){
                    String label = parser.getName();
                    if (label.equals("imagen")){
                        String image = parser.nextText();
                        int id_img = getResources().getIdentifier(image, "drawable", getPackageName());
                        this.layout.setBackgroundResource(id_img);
                    }
                }

                eventType = parser.next();
            }
        }catch (Throwable t){
            Toast.makeText(getApplicationContext(), "Error: DataFromXML", Toast.LENGTH_SHORT).show();
        }
    }
}
