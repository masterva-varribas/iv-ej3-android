/* Copyright (c) 2015
 * Author: Victor Arribas <v.arribas.urjc@gmail.com>
 * Domain: Master Vision Artificial, URJC
 * License: GPLv3 <http://www.gnu.org/licenses/gpl-3.0.html>
 */
 
package varribas.ejercicioiii;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class MainActivity extends ActionBarActivity implements AdapterView.OnItemClickListener{
    private ListView listView;
    private String[] lugares;
    private String[] xml;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);

        xml = getResources().getStringArray(R.array.resource_lugares_xml);

        // load data from xml resource and include for inflate
        lugares = getResources().getStringArray(R.array.resource_lugares);
        ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        // inject any other data
        adapter.addAll(lugares);
        adapter.add("Salir");
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Callback method to be invoked when an item in this AdapterView has
     * been clicked.
     * <p/>
     * Implementers can call getItemAtPosition(position) if they need
     * to access the data associated with the selected item.
     *
     * @param parent   The AdapterView where the click happened.
     * @param view     The view within the AdapterView that was clicked (this
     *                 will be a view provided by the adapter)
     * @param position The position of the view in the adapter.
     * @param id       The row id of the item that was clicked.
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position<lugares.length){
            String clicked = lugares[position];

            Intent i = new Intent(this, OpcionesLugar.class);
            i.putExtra("lugar", clicked);
            i.putExtra("xml", xml[position]);
            startActivity(i);

        }else{
            // aggregated exit entry
            this.finish();
        }
    }
}
